#ifndef MAT_OPS_FUNCTIONS_H
#define MAT_OPS_FUNCTIONS_H
#include<mpi.h>
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>
#define ROOT 0
#define TRUE 1
#define FALSE 0

typedef struct {
    int       p;         /* Total number of processes,comm_size */
    MPI_Comm  cart_comm; /* Communicator for entire grid */
    MPI_Comm  row_comm;  /* Communicator for my row      */
    MPI_Comm  col_comm;  /* Communicator for my col      */
    int       q;         /* Order of grid,  p_dim        */
    int       col_rank;    /* my col_rank     */
    int       row_rank;    /* my row_rank   */
    int       my_rank;   /* My rank in the grid comm     */
} GRID_INFO_T;


/*sets up a cartessian topology along with row and col communicators*/
void set_up_cart_topo(GRID_INFO_T* grid /*out*/);

/*reads a matrix  given a file name, allocates memory and returns
a pointer to the allocated matrix. caller needs to free memory */
double** read_mat(int n, char* file);

/*print matrix pointed by **mat with the specified dimension*/
void print_matrix(int rows,int cols, double **mat);

/*print the requested matrix from all the procs in sync */
void print_mat_sync(char *msg, GRID_INFO_T *grid, int cols, int rows, double** mat, MPI_Comm comm);

/* return a pointer to mat initialized dynamically in a contiguous block*/
double** init_2Darr_cont(int rows, int cols);

/* distribute glob mat to the grid populationg each loc */
void distribute_mat(int n,int loc_n, double **loc/*out*/,double **glob,GRID_INFO_T *grid);

/* add matrix A and B and  store sum in a loc_c in each processors. Then calls
*	combine_pieces  to populate glob_c
*/
void add_mats(GRID_INFO_T *grid, int loc_n, double **loc_a, double **loc_b, double **loc_c, double **glob_c);

/*combine pieces of loc spreaded in each processor into a glob_c allocated at the root*/
void combine_pieces(GRID_INFO_T *grid, int n, int loc_n, double **loc, double **glob);

/*add col_matrices to glob_c,  then print the result of the addition of each 
*row/col, add them together and then print the total sum 
*/
double row_col_add(GRID_INFO_T *grid,int loc_n, int n, char sum_rows, double **loc,double **loc_c, double ***glob_c);

/*multiply loc_a and loc_b of size loc_n x loc_n */
void Local_matrix_multiply(
         double**  local_A  /* in  */,
         double**  local_B  /* in  */, 
         double**  local_C  /* out */,
         int       loc_n    /*in */);
         
/* set a matrix of loc_n x loc_n to zeroes */
void Set_to_zero( 
         double**  local_A,  /* out */
         int loc_n   /*in */  );
        
/* uses fox's algorithm with stages to multiply 2 matrices, result is saved in
* Local_C mat of size loc_n x loc_n stored in each processor, after
* multiplication all loc_c matrices are
* combined into 1 glob_c mat allocated in the root node 
*/
void Fox(
				GRID_INFO_T *grid  /* in  */,
        int 				 n     /* in  */,
        int       loc_n   /* in  */,
        double**  local_A  /* in  */,
        double**  local_B  /* in  */,
        double**  local_C  /* out */);
        
#endif
