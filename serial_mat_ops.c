#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*
*usage  ./EXEC_NAME <matA/B_rows/cols>  <mat_A_filename> <mat_B_filename>  
* aguments are always required or program will exit with a return value of -1
*/



double** read_mat(int n, char* file);

void print_matrix(int n, double **mat);

double row_add_mat(double **mat, int n);

double col_add_mat(double **mat, int n);

double vec_product(double *A, double *B,  int n);

void transpose(double **vec,int n);

void mult_matrices(double **matA, double **matB, double **matC,int n);

void add_matrices(double **matA, double **matB, double **matC, int n);

int main(int argc, char** argv){
	if(argc == 1) return -1; //program exits if no command line arguments are given.
	int n = atoi( argv[1] );
	
	//initialize and fill matrices
	double **matA = read_mat(n,argv[2])
			 , **matB = read_mat(n,argv[3])
			 , **matC = (double**) malloc(sizeof(double*)*n);
	for(int i = 0; i < n; i++)
		matC[i] = (double*) malloc(sizeof(double)*n);
	
	printf("print matrix A\n");
	print_matrix(n,matA);
	putchar('\n');
	printf("print matrix B\n");
	print_matrix(n,matB);
	putchar('\n');
	
	printf("mult of A and B is \n");
	mult_matrices(matA, matB, matC, n);
	print_matrix(n,matC);
	putchar('\n');
	
	printf("sum of A and B is \n");
	add_matrices(matA, matB, matC, n);
	print_matrix(n,matC);
	putchar('\n');
	

	
	printf("row add is %f\n",row_add_mat(matA,n));
	
	printf("row add is %f\n",col_add_mat(matA,n));
	
	transpose(matA, n);
	print_matrix(n,matA);
	
	//free(matA);
	//free(matB);
	//free(matC);
	return 0;
}

double** read_mat(int n, char* file){
	double **mat;
	mat = (double**) malloc(sizeof(double**)*n);
	for(int i = 0; i < n; i++) mat[i] = (double*)malloc(sizeof(double)*n);
	FILE* fp;
	if( !( fp = fopen( file, "r") ) ){
		perror("");
		exit(-1);
	}

	for(int i = 0; i < n; i++)
		for(int j = 0; j < n; j++)
		fscanf(fp, "%lf",  &mat[i][j]);
	return mat;
}

void print_matrix(int n, double **mat){
	int i, j;
	for(i = 0; i < n; i++){
		for(j = 0; j < n; j++)
			printf(" %.1lf \t", mat[i][j]);
		putchar('\n');
	}
			
}

double row_add_mat(double **mat,int n){
	double total_sum=0;
	double row_sum=0;
	for(int i= 0 ; i < n; i++){
		for(int j = 0; j < n; j++)
			row_sum += mat[j][i];
		total_sum += row_sum;
		row_sum = 0;
	}
	
	return total_sum;
}

double col_add_mat(double **mat,int n){
	double total_sum=0;
	double row_sum=0;
	for(int i= 0 ; i < n; i++){
		for(int j = 0; j < n; j++)
			row_sum += mat[i][j] ;
		total_sum += row_sum;
		row_sum = 0;
	}
	
	return total_sum;
}

double vec_product(double *A, double *B,  int n){
	double result = 0.0;
	for(int i = 0; i < n; i++)
		result += A[i] + B[i];

}

//transposes an n*n matrix, doesnt work for square matrices
void transpose(double **vec,int n){
	for(int i = 0; i < n ; i++)
		for(int j = i; j < n; j++){
			double temp = vec[i][j];
			vec[i][j] = vec[j][i];
			vec[j][i] = temp;
		}
			
}

void mult_matrices(double **matA, double **matB, double **matC,int n){ //*matA +i*n +k -1)
	for(int i = 0; i < n; i++)
		for(int j = 0; j < n; j++)
			for(int k = 0; k < n; k++){
				matC[i][j] += matA[i][k] * matB[k][j];
			}
}
void add_matrices(double **matA, double **matB, double **matC, int n){
	for(int i= 0 ; i < n; i++)
		for(int j = 0; j < n; j++)
			matC[i][j] = matA[i][j] + matB[i][j]; 
}





