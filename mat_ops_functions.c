#include "mat_ops_functions.h"

/*sets up a cartessian topology along with row and col communicators*/
void set_up_cart_topo(
         GRID_INFO_T* grid /*out*/){
	int dim[2] /*array containing size of dimensions*/
    ,wrap_around[2] /*logical array. wrap around of each dim */
    ,reorder /*reorder nodes to optimize 2d topo */
    ,free_coords[2];
	/*set up cart_comm*/
  MPI_Comm_size(MPI_COMM_WORLD, &(grid->p));
  grid->q = (int)sqrt((double)grid->p);
  dim[0] = dim[1] = grid->q ;//sets up size of dimensions. 
  wrap_around[0] = wrap_around[1] = 1;
  reorder = 1;
  MPI_Cart_create(MPI_COMM_WORLD,2,dim,wrap_around,reorder,&(grid->cart_comm));
  //MPI_Comm_rank(grid->cart_comm, &(grid->my_rank));
  
	/*set up row_comm and col_comm */
  free_coords[0] = 0; free_coords[1] = 1;
  MPI_Cart_sub( grid->cart_comm, free_coords, &(grid->row_comm) );
  free_coords[0] = 1; free_coords[1] = 0;
   /* get row_rank and column_rank of each processor */
  MPI_Cart_sub(grid->cart_comm, free_coords, &(grid->col_comm));
  MPI_Comm_rank(grid->row_comm, &(grid->row_rank));
  MPI_Comm_rank(grid->col_comm, &(grid->col_rank));
}
         

/*reads a matrix  given a file name, allocates memory and returns
a pointer to the allocated matrix. caller needs to free memory */
double** read_mat(int n, char* file){
	double **mat = init_2Darr_cont(n,n);
	//mat = (double**) malloc(sizeof(double*)*n);
	//mat[0] = (double*) malloc(n*n*sizeof(double));
	for(int i = 1; i < n; i++) mat[i] = mat[0] + i *n;
	
	FILE* fp = fopen( file, "r");
	if( ! fp  ){ // unable to read file
		perror("");
		exit(-1);
	}
	// read matrix from the file 
	for(int i = 0; i < n; i++)
		for(int j = 0; j < n; j++)
		  fscanf(fp, "%lf",  &mat[i][j]);
		  
		
	fclose(fp);
	return mat;
}//end read_mat

/*print matrix pointed by **mat with the specified dimension*/
void print_matrix(int rows,int cols, double **mat){
	int i, j;
	for(i = 0; i < rows; i++){
		for(j = 0; j < cols; j++)
			printf(" %.1lf \t", mat[i][j]);
		putchar('\n'); // separate each row with a newline
	}
			
}//end read_mat 

/*print the requested matrix from all the procs in sync */
void print_mat_sync(char *msg, GRID_INFO_T *grid, int cols, int rows, double** mat, 
MPI_Comm comm){
		if(grid->my_rank == ROOT)
			printf("%s*******************************\n",msg);
	
		for(int i = 0; i < grid->p; i++){
			if(i == grid->my_rank){
				printf("process %d , coords (%d , %d)\n" ,
            grid->my_rank, grid->col_rank, grid->row_rank);
				print_matrix(cols,rows, mat);
	 		}
	 		MPI_Barrier(comm);
	 	}
    MPI_Barrier(comm);
	 	if(grid->my_rank == (grid->p)-1)
			printf("END%s*******************************\n",msg);
}

/* return a pointer to mat allocated dynamically in a contiguous block*/
double** init_2Darr_cont(int rows, int cols){
	double **mat = (double**) malloc(sizeof(double*)*rows);// pointers for each row
	mat[0] = (double*) malloc(sizeof(double)*rows*cols);
	for(int i = 1; i < rows; i++)
		mat[i] = mat[0]+i*cols;
	return mat;
}//end init_2Darr_cont

/* distribute glob mat to the grid population each local matrix  */
void distribute_mat(int n, int loc_n, double **loc/*out*/,double **glob, GRID_INFO_T *grid){  
	//holds loc_n rows of  n columns for each processor in column 0
  double **temp_rows =  init_2Darr_cont(loc_n,n);

 if(grid->row_rank == 0){//first column
    if(grid->my_rank == ROOT)
      MPI_Scatter(glob[0],loc_n*n,MPI_DOUBLE,temp_rows[0],loc_n*n,MPI_DOUBLE,0,grid->col_comm);
    else
      MPI_Scatter(NULL,loc_n*n,MPI_DOUBLE,temp_rows[0],loc_n*n,MPI_DOUBLE,0,grid->col_comm);
  }
   
  MPI_Barrier(grid->cart_comm); // wait for the scatter

  
  if(grid->row_rank == 0){//first column. transmit horizontally
    for(int i = 0; i < loc_n; i++)
      MPI_Scatter(temp_rows[i], loc_n, MPI_DOUBLE ,loc[i],loc_n,MPI_DOUBLE,0,grid->row_comm);

 }else{//rest of the columns, receive from first col
  for(int i = 0; i < loc_n; i++)
 		MPI_Scatter(NULL,loc_n,MPI_DOUBLE ,loc[i],loc_n,MPI_DOUBLE,0,grid->row_comm);
  }
  /* free temp_rows */
  //free( temp_rows[0] ); // the data 
  //free( temp_rows ) ; //the pointers
  

}	/****************END distribute_mat ***************************************/

/* add matrix A and B and  store sum in a loc_c in each processors. Then calls
*	combine_pieces  to populate glob_c
*/
void add_mats(GRID_INFO_T *grid, int loc_n, double **loc_a, double **loc_b, double 
**loc_c, double **glob_c){  
	//adding. result will be located at the loc_c of each procs
	for(int i = 0 ; i  < loc_n; i++)
		for(int j = 0 ; j < loc_n; j++)
			loc_c[i][j] = loc_a[i][j] + loc_b[i][j];

}/**************************END add_mats ***********************************/ 

/*combine pieces of loc spreaded in each processor into a glob_c allocated at the root*/
void combine_pieces(GRID_INFO_T *grid, int n, int loc_n, double **loc, double **glob){ 
  double **temp_rows;
	
	// collect loc of a row  in temp_rows the first column(first and last Gather)
  // then collect all temp_rows in the glob array of the root (2 inner Gathers()
  if(grid->row_rank == 0) {
    temp_rows = init_2Darr_cont(loc_n,n);
    MPI_Barrier(grid->col_comm);
    for(int i = 0; i < loc_n; i++)
      MPI_Gather(loc[i], loc_n, MPI_DOUBLE ,temp_rows[i], loc_n, MPI_DOUBLE,0,grid->row_comm);
    if(grid->my_rank == ROOT){
      MPI_Gather(temp_rows[0], loc_n*n, MPI_DOUBLE ,glob[0], loc_n*n, MPI_DOUBLE, 0, grid->col_comm);
    }else
      MPI_Gather(temp_rows[0], loc_n*n, MPI_DOUBLE ,NULL, loc_n*n, MPI_DOUBLE, 0, grid->col_comm);

  }else{
    for(int i = 0; i < loc_n; i++)
      MPI_Gather(loc[i], loc_n, MPI_DOUBLE ,NULL, loc_n, MPI_DOUBLE,0,grid->row_comm);
  }
	

}/***************** END combine_pieces **************************************/



/*add col_matrices to glob_c,  then print the result of the addition of each 
*row/col, add them together and then print the total sum 
*/
double row_col_add(GRID_INFO_T *grid,int loc_n, int n, char sum_rows, double **loc, 
double **loc_c, double ***glob_c){
  if(grid->my_rank == ROOT){
    if( sum_rows) printf("adding the rows of a matrix\n");
    else printf("adding the columns of a matrx \n");	
   }
	MPI_Comm com1 = sum_rows?grid->row_comm:grid->col_comm
					 ,com2 = sum_rows?grid->col_comm:grid->row_comm;
	int rank = sum_rows? grid->row_rank: grid->col_rank;
	
	double sum ;
	for(int j = 0; j < loc_n; j++){
		sum = 0;
		for(int i = 0; i < loc_n; i++)
		sum += sum_rows?loc[j][i]:loc[i][j];
			
		MPI_Reduce(&sum, &loc_c[0][j], 1, MPI_DOUBLE,MPI_SUM,0,com1);
				
	}
	double **temp_glob = init_2Darr_cont(n,1);
	if(rank == 0)//pass it on to the root 
	 	MPI_Gather(loc_c[0],loc_n,MPI_DOUBLE
					 ,temp_glob[0],loc_n,MPI_DOUBLE,0,com2);
	if(grid->my_rank == ROOT){
		sum= 0;
		for(int i= 0; i < n; i++)
			sum += temp_glob[0][i];
	}
	*glob_c = temp_glob;
	return sum;
}/**********************END row_col_add *************************************/

/*multiply loc_a and loc_b of size loc_n x loc_n */
void Local_matrix_multiply(
         double**  local_A  /* in  */,
         double**  local_B  /* in  */, 
         double**  local_C  /* out */,
         int       loc_n    /*in */) {
    int i, j, k;

    for (i = 0; i < loc_n; i++)
        for (j = 0; j < loc_n; j++)
            for (k = 0; k < loc_n; k++)
            		local_C[i][j] += local_A[i][k]*local_B[k][j];

}  /*****************END Local_matrix_multiply ******************************/

/* set a matrix of loc_n x loc_n to zeroes */
void Set_to_zero(
         double**  local_A,  /* out */
         int loc_n   /*in */  ) {

    int i, j;

    for (i = 0; i < loc_n; i++)
        for (j = 0; j < loc_n; j++)
            local_A[i][j] = 0.0;

}/***************** END Set_to_zero *****************************************/

/* uses fox's algorithm with stages to multiply 2 matrices, result is saved in
* loc_c mat of size loc_n x loc_n stored in each processor, after
* multiplication all loc_c matrices are
* combined into 1 glob_c mat allocated in the root node 
*/
void Fox(
				GRID_INFO_T *grid  /* in  */,
        int 				 n     /* in  */,
        int       loc_n   /* in  */,
        double**  local_A  /* in  */,
        double**  local_B  /* in  */,
        double**  local_C  /* out */) {

    double**  temp_A; /* Storage for the sub-    */
                             /* matrix of A used during */ 
                             /* the current stage       */
    int              stage;
    int              bcast_root;
    //int              n_bar;  /* n/sqrt(p)               */
    int              source;
    int              dest;
    MPI_Status       status;

    //n_bar = n/grid->q;
    Set_to_zero(local_C, loc_n);
		MPI_Barrier(MPI_COMM_WORLD);

    /* Calculate addresses for circular shift of B */  
    source = (grid->col_rank  + 1) % grid->q;
    dest = (grid->col_rank + grid->q - 1) % grid->q;

    /* Set aside storage for the broadcast block of A */
    temp_A = init_2Darr_cont(loc_n, loc_n );

    for (stage = 0; stage < grid->q; stage++) {
        bcast_root = (grid->col_rank + stage) % grid->q;
        if(bcast_root == grid->row_rank) {
            MPI_Bcast(local_A[0], loc_n*loc_n, MPI_DOUBLE,bcast_root, grid->row_comm);
            Local_matrix_multiply(local_A, local_B, local_C, loc_n);
        }else {
            MPI_Bcast(temp_A[0], loc_n*loc_n, MPI_DOUBLE,bcast_root, grid->row_comm);
            Local_matrix_multiply(temp_A, local_B, local_C, loc_n);
        }
        MPI_Sendrecv_replace(local_B[0], loc_n*loc_n, MPI_DOUBLE, dest, 0, source, 0, 
grid->col_comm, &status);
    } /* for */
    
} /************************* END  Fox ***************************************/


                                

