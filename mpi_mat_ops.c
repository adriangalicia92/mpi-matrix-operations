#include "mat_ops_functions.h"
/* now assume only square matrices are allowed.
 * usage: mpirun -np <procs> ./EXEC_NAME  <N_rows/cols> <mat_A_filename> <mat_B_filename> <operation> [verbose]
 * operation can be:
 * -mul     : matrix multiplication
 * -sum     : mat addition),
 * -Arowadd : row addition of matrix A
 * -Acoladd : column addition of matrix A
 * -Browadd : row addition of matrix B
 * -Bcoladd : column addition of matrix B
 *
 * <> denotes required arguments
 * [] denotes optional arguments
 */
typedef enum { false=0 , true}bool; // boolean


int main(int argc, char *argv[]){
  GRID_INFO_T *grid = (GRID_INFO_T*)malloc(sizeof(GRID_INFO_T));

/********************* initialize MPI environment *********************/
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &(grid->my_rank));
/********************* Check command line Arguments *******************/
  if(  argc < 5){
    if( grid->my_rank == ROOT ) {
      fprintf(stderr, "usage: mpirun -np <procs> %s  <N_rows/cols>/ <mat_A_filename> <mat_B_filename> <operation> [verbose] \n",argv[0]);
      fprintf(stderr, " ARGUMENTS ARE REQURIED \n");
      fprintf(stderr, "operation can be: mul(matrix multiplication), sum(mat addition), Arowadd Acoladd Browadd Bcoladd \n");
    }
    MPI_Finalize();
    return -1;
  }

/********************* Variable Declarations for each process**********/

  int loc_n     /* dim of local arrays, n/sqrt(comm_size)*/
    ,n = atoi(argv[1]) ;//matrices dimensions(assume sq mat)
  char* operation = argv[4];// operation to perform on the matrices
  bool verbose = argv[5]? true:false;
  /* pointers to store all the matrices and local matrices */
  double  **glob_a=NULL, **glob_b=NULL, **glob_c=NULL, **loc_a, **loc_b, **loc_c;

  double tstart=0.0 ; /* variable to time the program */

/********************* Set up Topology ********************************/
  tstart = MPI_Wtime();
  set_up_cart_topo(grid);//saves everything in each process 'grid' variable.
  MPI_Barrier(MPI_COMM_WORLD);
  if(grid->my_rank==ROOT) printf("Time to set_up cartessian topology is %lf\n", MPI_Wtime() - tstart);

  //sets up dims of each loc matrix(loc_n x loc_n)
  loc_n = n /  grid->q;
  if(grid->my_rank == ROOT){
    printf(" n = %d \n", n);
    printf("loc_n = %d\n", loc_n);
    printf("p_dim = %d\n", grid->q);
  }

/**Initialize local arrays to be of size loc_n by loc_n, contiguously allocated */
  loc_a  = init_2Darr_cont(loc_n, loc_n);
  loc_b  = init_2Darr_cont(loc_n, loc_n);
  loc_c  = init_2Darr_cont(loc_n, loc_n);


/****** Populate matrices glob  A & B with file in arguments **********/
  if(grid->my_rank == ROOT){
    glob_a = read_mat(n,argv[2]);
    glob_b = read_mat(n,argv[3]);
  }
  MPI_Barrier(MPI_COMM_WORLD);

/********** Print Matrices A and B ************************************/
  if(grid->my_rank== ROOT){
    printf("I am the root \n");
    if( verbose ){
      printf("this is matrix A \n");
      print_matrix(n,n,glob_a);
      putchar('\n');
      printf("this is matrix B \n");
      print_matrix(n,n,glob_b);
      putchar('\n');
    }
  }

/*******************  data distribution *******************************/
  tstart = MPI_Wtime();
  // Distribute Matrix A
  distribute_mat(n, loc_n, loc_a, glob_a, grid);
  MPI_Barrier(MPI_COMM_WORLD);

  // Distribute Matrix B
  distribute_mat(n, loc_n, loc_b, glob_b, grid);
  MPI_Barrier(MPI_COMM_WORLD);

  if(grid->my_rank==ROOT)printf("Time to distribute matrices is  %lf\n", MPI_Wtime() - tstart);

#ifdef DEBUG2
  print_mat_sync("printing loc_b ",grid,loc_n,loc_n,loc_b,MPI_COMM_WORLD);
#endif
  if(grid->my_rank == ROOT)
    puts("matrix distribution completed ");
  
  MPI_Barrier(MPI_COMM_WORLD);

/********************  Add matrix A and matrix B  *********************/
  if(strstr(operation,"sum") != NULL ){
    tstart = MPI_Wtime();
    add_mats(grid,loc_n, loc_a, loc_b, loc_c, glob_c);
    MPI_Barrier(MPI_COMM_WORLD);

    if(grid->my_rank==ROOT)printf("Time to Add two matrices is  %lf\n", MPI_Wtime() - tstart);
    glob_c = init_2Darr_cont(n,n);
    //combining loc_c results into  into glob_c// 1 mpi gather doesnt work
    combine_pieces(grid,n,loc_n, loc_c,glob_c);
    MPI_Barrier(MPI_COMM_WORLD);

    if(grid->my_rank ==ROOT){
      printf("SUM of A and B is \n");
      if( verbose)
        print_matrix(n,n,glob_c);
    }
#ifdef DEBUG2
    print_mat_sync("printing loc_c mats after mult",grid,loc_n,loc_n,loc_c, MPI_COMM_WORLD);
#endif
  }
/********************  (row/col)_add of mat (A/B)  ********************/

  if(strstr(operation,"add") != NULL ){
    //double **loc= (double**)malloc(sizeof(double*)*loc_n);
    bool add_rows; char dir[8],mat[9]; double sum;

    if(strstr(argv[4],"row") != NULL){//add the rows
      add_rows = true;
      strcpy(dir,"rows");

    }else{    //add the columns
      add_rows = false;
      strcpy(dir,"columns");

    }

    if( strstr(operation,"A") != NULL ){  //work on matrix A
      tstart = MPI_Wtime();
      sum =  row_col_add(grid, loc_n, n, add_rows, loc_a, loc_c, &glob_c);
      MPI_Barrier(MPI_COMM_WORLD);

      if(grid->my_rank==ROOT) printf("Time add the %s  is  %lf\n",dir, MPI_Wtime() - tstart);
      strcpy(mat, "Matrix A");

    } else{                               //work on matrix B
      tstart = MPI_Wtime();
      sum =  row_col_add(grid, loc_n, n, add_rows, loc_b, loc_c, &glob_c);
      MPI_Barrier(MPI_COMM_WORLD);
      if(grid->my_rank==ROOT) printf("Time add the %s  is  %lf\n",dir, MPI_Wtime() - tstart);
      strcpy(mat, "Matrix B");

    }

    if(grid->my_rank == ROOT){
      printf("%s  of %s sum is: \n", dir,mat);
      if (verbose )
        print_matrix(1,n,glob_c);
      printf(" sum of %s of %s is = %.1lf\n",dir,mat,sum);

    }
  }

/******************** START  mul mat A and mat B   ********************/
  if(strstr(operation,"mul") != NULL){
#ifdef DEBUG2
    print_mat_sync("printing loc_c before   mult",grid,loc_n,loc_n,loc_c, MPI_COMM_WORLD);
#endif
    tstart = MPI_Wtime();
    Fox(grid, n, loc_n, loc_a, loc_b, loc_c);
    MPI_Barrier(MPI_COMM_WORLD);

    if(grid->my_rank==ROOT)printf("Time multiply two matrices  is  %lf\n", MPI_Wtime() - tstart);

    //combining loc_c results into  into glob_c// 1 mpi gather doesnt work
    glob_c = init_2Darr_cont(n,n);
    combine_pieces(grid,n, loc_n, loc_c, glob_c);
    MPI_Barrier(MPI_COMM_WORLD);

#ifdef DEBUG2
    print_mat_sync("printing loc_c mats after mult",grid,loc_n,loc_n,loc_c, MPI_COMM_WORLD);
#endif
    if(grid->my_rank ==ROOT){
      printf("The product of Matrix A and Matrix B is\n");
      if (verbose )
       print_matrix(n,n,glob_c);
    }
  }

  MPI_Finalize();
  return 0;

}/* ******************************end main *******************************  */



