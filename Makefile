CC = gcc
MCC = mpicc
CCFLAGS = -std=gnu99 -lm -Wall -Wshadow $(FLAG)

all: serial_mat_ops  mpi_mat_ops fox

serial_mat_ops:  serial_mat_ops.c
	$(CC) $(CCFLAGS) -o serial_mat_ops serial_mat_ops.c 

mpi_mat_ops: mpi_mat_ops.c mat_ops_functions.h mat_ops_functions.c
	$(MCC) -c mat_ops_functions.c $(CCFLAGS)
	$(MCC) -c mpi_mat_ops.c $(CCFLAGS)
	$(MCC) -o mpi_mat_ops mpi_mat_ops.o mat_ops_functions.o $(CCFLAGS)

fox: fox.c
	$(MCC) -o fox fox.c $(CCFLAGS)

clean:
	rm -rf serial_mat_ops create_mat fox *.o

run:
	mpirun -np $(NPROCS) ./mpi_mat_ops $N input/A_$Nx$N.dat input/B_$Nx$N.dat ${OPERATION} 
