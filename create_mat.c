#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>



int main(int argc, char** argv) {
	if(argc < 3){
		fprintf(stderr,"Usage: %s size file \n", argv[0]);
		exit(-1);
	}
	int size = atoi(argv[1]);
	FILE* fp = fopen(argv[2], "w");
	if( !fp ){
		perror("create: unable to create file");
	}
	
	int i, j,
	k= argv[3]? (int)pow(size,2) : 1;
	printf("k= %d\n",k);
	for(i = 0; i < size ; i ++){
		for( j = 0; j < size; j ++){
			fprintf(fp, "%d ", k);
			k = argv[3]? k-1:k+1;
		}
		putc( '\n',fp);
		
	}
	printf(" %s created \n", argv[2]);
	fclose(fp);
	
}
